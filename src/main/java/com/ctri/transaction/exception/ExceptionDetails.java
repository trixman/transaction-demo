package com.ctri.transaction.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ExceptionDetails {

    private Date timestamp;

    @Getter
    @Setter
    private String message;

    @Getter
    @Setter
    private String description;

    public Date getTimestamp() {
        return null != this.timestamp ? new Date(this.timestamp.getTime()) : null;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = null != timestamp ? new Date(timestamp.getTime()) : null;
    }
}
