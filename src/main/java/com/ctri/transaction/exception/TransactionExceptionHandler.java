package com.ctri.transaction.exception;

import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
@RestController
@Slf4j
class TransactionExceptionHandler extends ResponseEntityExceptionHandler {

    @Value("${messages.conflict}")
    private String conflict;
    @NotNull
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ExceptionDetails> handleAll(Exception ex, WebRequest req) {
        var details = new ExceptionDetails(new Date(), ex.getMessage(),
                req.getDescription(false));
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @NotNull
    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<ExceptionDetails> handleNotFound(NotFoundException ex, WebRequest req) {
        var details = new ExceptionDetails(new Date(), ex.getMessage(),
                req.getDescription(false));
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @NotNull
    @ExceptionHandler(BadRequestException.class)
    public final ResponseEntity<ExceptionDetails> handleBadRequest(BadRequestException ex, WebRequest req) {
        var details = new ExceptionDetails(new Date(), ex.getMessage(),
                req.getDescription(false));
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
    }

    @NotNull
    @ExceptionHandler(OptimisticLockingFailureException.class)
    public final ResponseEntity<ExceptionDetails> handleOptimisticLockingFailure(@NotNull Exception ex, @NotNull WebRequest request) {
        var exceptionDetails = new ExceptionDetails(new Date(), conflict,
                request.getDescription(false));
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(exceptionDetails, HttpStatus.CONFLICT);
    }

}
