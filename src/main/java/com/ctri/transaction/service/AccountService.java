package com.ctri.transaction.service;

import com.ctri.transaction.model.Account;
import com.ctri.transaction.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public Flux<Account> findAll() {
        return accountRepository.findAll();
    }

    public Mono<Account> findById(UUID id) {
        return accountRepository.findById(id);
    }

    @Transactional
    public Mono<Account> create(Account account) {
        return accountRepository.save(account);
    }

    @Transactional
    public Mono<Account> update(Account account) { return accountRepository.save(account); }

}
