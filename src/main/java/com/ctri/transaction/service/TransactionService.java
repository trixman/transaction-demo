package com.ctri.transaction.service;

import com.ctri.transaction.component.AccountUtils;
import com.ctri.transaction.exception.BadRequestException;
import com.ctri.transaction.exception.NotFoundException;
import com.ctri.transaction.model.Transaction;
import com.ctri.transaction.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static java.text.MessageFormat.format;

@Service
public class TransactionService {

    @Value("${messages.not-found}")
    private String notFound;

    @Value("${messages.bad-request.same-account}")
    private String sameAccount;

    @Value("${messages.bad-request.insufficient-balance}")
    private String insufficientBalance;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountUtils accountUtils;

    @Autowired
    private TransactionRepository transactionRepository;

    public Flux<Transaction> findAll() {
        return transactionRepository.findAll();
    }

    public Mono<Transaction> findById(UUID id) {
        return transactionRepository.findById(id);
    }

    @Transactional
    public Mono<Transaction> create(Transaction transaction) {
        handleAccounts(transaction);
        return transactionRepository.save(transaction);
    }

    private void handleAccounts(Transaction transaction) {
        var dbSourceAccount = accountService.findById(transaction.getSourceAccountId())
                .blockOptional().orElseThrow(() -> new NotFoundException(format(notFound, transaction.getSourceAccountId())));

        if (transaction.getSourceAccountId().equals(transaction.getTargetAccountId())) {
            throw new BadRequestException(format(sameAccount));
        }
        if (dbSourceAccount.getBalance().compareTo(transaction.getAmount()) < 0) {
            throw new BadRequestException(format(insufficientBalance));
        }

        var dbTargetAccount = accountService.findById(transaction.getTargetAccountId())
                .blockOptional().orElseThrow(() -> new NotFoundException(format(notFound, transaction.getSourceAccountId())));

        accountService.update(
                accountUtils.prepareSourceAccount(dbSourceAccount, transaction.getSourceAccountVersion(), transaction)).block();
        accountService.update(
                accountUtils.prepareTargetAccount(dbTargetAccount, transaction.getTargetAccountVersion(), transaction)).block();
    }

}