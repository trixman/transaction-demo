package com.ctri.transaction.repository;

import com.ctri.transaction.model.Transaction;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import java.util.UUID;

public interface TransactionRepository extends ReactiveCrudRepository<Transaction, UUID> {
	
}
