package com.ctri.transaction.repository;

import com.ctri.transaction.model.Account;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import java.util.UUID;

public interface AccountRepository extends ReactiveCrudRepository<Account, UUID> {

}
