package com.ctri.transaction.component;

import com.ctri.transaction.model.Currency;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CurrencyUtils {

    public BigDecimal convert(Currency target, Currency source, BigDecimal amount) {
        if (target.equals(source)) {
            return amount;
        }
        return amount;
    }
}
