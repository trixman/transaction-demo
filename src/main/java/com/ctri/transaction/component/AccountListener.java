package com.ctri.transaction.component;

import com.ctri.transaction.model.Account;
import org.reactivestreams.Publisher;
import org.springframework.data.mongodb.core.mapping.event.ReactiveBeforeConvertCallback;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.UUID;

@Component
public class AccountListener implements ReactiveBeforeConvertCallback<Account> {

    @Override
    public Publisher<Account> onBeforeConvert(Account entity, String collection) {
        if (null == entity.getId()) {
            entity.setId(UUID.randomUUID());
        }
        if (null == entity.getCreatedAt()) {
            entity.setCreatedAt(new Date());
        } else {
            entity.setUpdatedAt(new Date());
        }
        return Mono.just(entity);
    }
}