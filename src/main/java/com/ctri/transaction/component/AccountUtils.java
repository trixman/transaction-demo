package com.ctri.transaction.component;

import com.ctri.transaction.model.Account;
import com.ctri.transaction.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountUtils {

    @Autowired
    private CurrencyUtils currencyUtils;

    public Account prepareSourceAccount(Account dbAccount, Integer version, Transaction transaction) {
        var amount = currencyUtils.convert(dbAccount.getCurrency(), transaction.getCurrency(), transaction.getAmount());
        var account = prepareAccount(dbAccount, version);
        account.setBalance(dbAccount.getBalance().subtract(amount));
        return account;
    }

    public Account prepareTargetAccount(Account dbAccount, Integer version, Transaction transaction) {
        var amount = currencyUtils.convert(dbAccount.getCurrency(), transaction.getCurrency(), transaction.getAmount());
        var account = prepareAccount(dbAccount, version);
        account.setBalance(dbAccount.getBalance().add(amount));
        return account;
    }

    private Account prepareAccount(Account dbAccount, Integer version) {
        var account = new Account();
        account.setId(dbAccount.getId());
        account.setCurrency(dbAccount.getCurrency());
        account.setCreatedAt(dbAccount.getCreatedAt());
        account.setUpdatedAt(dbAccount.getUpdatedAt());
        account.setVersion(version);
        return account;
    }

}