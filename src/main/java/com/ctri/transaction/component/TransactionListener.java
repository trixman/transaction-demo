package com.ctri.transaction.component;

import com.ctri.transaction.model.Transaction;
import org.reactivestreams.Publisher;
import org.springframework.data.mongodb.core.mapping.event.ReactiveBeforeConvertCallback;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.UUID;

@Component
public class TransactionListener implements ReactiveBeforeConvertCallback<Transaction> {

    @Override
    public Publisher<Transaction> onBeforeConvert(Transaction entity, String collection) {
        if (null == entity.getId()) {
            entity.setId(UUID.randomUUID());
        }
        if (null == entity.getCreatedAt()) {
            entity.setCreatedAt(new Date());
        }
        return Mono.just(entity);
    }
}