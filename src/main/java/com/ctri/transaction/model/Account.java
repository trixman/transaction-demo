package com.ctri.transaction.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Document
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Account {

	@Id
	private UUID id;

	@NotNull(message = "Missing account balance")
	@DecimalMin(value = "0.00", message = "Invalid account balance")
	@Digits(integer = 10, fraction = 2, message = "Invalid account balance")
	private BigDecimal balance;

	@NotNull(message = "Missing currency")
	private Currency currency;

	@CreatedDate
	private Date createdAt;

	@LastModifiedDate
	private Date updatedAt;

	@Version
	private Integer version;
}
