package com.ctri.transaction.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Document
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Transaction {

	@Id
	private UUID id;

	@NotNull(message = "Missing source account id")
	private UUID sourceAccountId;

	@Transient
	private Integer sourceAccountVersion;

	@NotNull(message = "Missing target account id")
	private UUID targetAccountId;

	@Transient
	private Integer targetAccountVersion;

	@NotNull(message = "Missing transaction amount")
	@DecimalMin(value = "0.0", inclusive = false, message = "Invalid transaction amount")
	@Digits(integer = 6, fraction = 2, message = "Invalid transaction amount")
	private BigDecimal amount;

	@NotNull(message = "Missing currency")
	private Currency currency;

	@CreatedDate
	private Date createdAt;
}
