package com.ctri.transaction.controller;

import com.ctri.transaction.component.AccountUtils;
import com.ctri.transaction.model.Account;
import com.ctri.transaction.service.AccountService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/accounts")
@Slf4j
public class AccountController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private AccountUtils accountUtils;

	@GetMapping(produces = APPLICATION_JSON_VALUE)
	public Flux<Account> findAll() {
		log.info("findAll");
		return accountService.findAll();
	}

	@GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
	public Mono<Account> findById(@PathVariable("id") UUID id) {
		log.info("findById: id={}", id);
		return accountService.findById(id);
	}

	@PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	public Mono<Account> create(@Valid @RequestBody Account account) {
		log.info("create: {}", account);
		return accountService.create(account);
	}
	
}
