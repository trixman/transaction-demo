package com.ctri.transaction.controller;

import com.ctri.transaction.component.AccountUtils;
import com.ctri.transaction.model.Transaction;
import com.ctri.transaction.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/transactions")
@Slf4j
public class TransactionController {

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private AccountUtils accountUtils;

	@GetMapping(produces = APPLICATION_JSON_VALUE)
	public Flux<Transaction> findAll() {
		log.info("findAll");
		return transactionService.findAll();
	}

	@GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
	public Mono<Transaction> findById(@PathVariable("id") UUID id) {
		log.info("findById: id={}", id);
		return transactionService.findById(id);
	}

	@PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	public Mono<Transaction> create(@RequestBody Transaction transaction) {
		log.info("create: {}", transaction);
		return transactionService.create(transaction);
	}
	
}
