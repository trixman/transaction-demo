FROM openjdk:17-jdk-slim
ARG DEPENDENCY=build/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT ["java","-XX:InitialRAMPercentage=70","-XX:MaxRAMPercentage=70","-XX:+UseContainerSupport","-XX:InitialHeapSize=0","-XshowSettings:vm","-cp","app:app/lib/*","com.ctri.transaction.TransactionApplication"]


