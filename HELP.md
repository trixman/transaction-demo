# Running the demo locally

## Prerequisites
- Make sure there is a **Java-17 SDK** installation in your local environment
- Make sure there is a **Gradle-7.6** installation in your local environment
- Make sure there is a **Postman** installation in your local environment
- Make sure there is sufficient **Docker** infrastructure in your local environment

## Next Steps
1. Clone the transaction-demo Git repository:
   1. **SSH** `git@gitlab.com:trixman/transaction-demo.git`
   2. **HTTPS** `https://gitlab.com/trixman/transaction-demo.git`
2. Navigate to the local repository directory
3. Modify the `MONGO_DATA_HOST_PATH` and `MONGO_LOG_HOST_PATH` in the `.env` file to match your preferences
4. Run `gradle clean build docker` to create the `transaction-demo` docker image
5. Run `docker network create transaction-demo-network`
6. Run `docker-compose --env-file ".\.env" build`
7. Run `docker-compose --env-file ".\.env" up -d`
8. Import the `.\postman\Transaction-Demo.postman_collection.json` to Postman and start firing requests against `localhost:8000`
9. Create accounts using the `sample_create_account.json` payload accordingly
10. Create transactions using the `sample_create_transaction.json` payload accordingly
11. When creating a transaction, the `sourceAccountVersion` and `targetAccountVersion` fields must be set to the current value of the source and target accounts' `version` field respectively, otherwise an optimistic locking failure will occur. 


 


